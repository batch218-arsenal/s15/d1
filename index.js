console.log("Hello world!");
// remember that JavaScript is case sensitive

console. log("Hello world!");
// not sensitive to spaces

console.
log
(
	"Hello, everyone!"
)

//  ; delimeter
// we use delimeter to end our code

//  [COMMENTS]
//  - single line comments, shortcut: ctrl + /

// I am a single line comment

/* Multi-line comment, shortcut: ctrl + shift + /*/
/*
	I am
	a
	multi line
	comment
*/

// Syntax and statement

// Statements in programming are instructions that we tell the computer to perform
// Syntax in programming is the set of rules that describes how statements must be considered

// Variables
/*
	it is used to contain data

	-Syntax in declaring variables
	-let/const VariableName
*/

let myVariable = "Hello";

let wallet = 20;

console.log(myVariable);

// console.log(hello); will result to not defined error

/*
	Guides in writing variables:
		1. Use the "let" keyword followed by the desired variable name and use the equal sign (=) to assign a value to the variable
		2. Variable names should start with a lowercase character, use camelCase for multiple wordded variable names: example - natnatGwapo
		3. For constant variables, use the "const" keyword
		4. Variable names should describe its assigned value
		5. Never name a variable starting with numbers
		6. Refrain from usince spaces in declaring a variable.

*/

// values for variables can be assigned using single quotes ('') or double quotes ("")
let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer"
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// Reassigning a variable's value
// Syntax
	// variableName = newValue;

productName = "Laptop"
console.log(productName)

let friend = "Rose";
friend = "Diane";
console.log(friend);

// interest = 5.68;
// reassigning a value to a const variable produces an error

let supplier; // declaration
supplier = "John Smith Tradings"; // initializing
console.log(supplier);

supplier = "Zuitt Store"; //reassigning
console.log(supplier);

//Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with a reserved keyword produces an error
/* 
	const let = "hello";
	console.log(let);
*/

// [SECTION] Data Types

/* Strings - are series of characters that create a word, phrase, sentence, or anything related to creating text.

		   - a string is enclosed with single (' ') or double (" ") quotes
*/

let country = 'Philippines';
let province = "Metro Manila";

let fullAddress = province + ', ' + country;
console.log(province + ', ' + country);
console.log(fullAddress);
console.log('Metro Manila' + ', ' + "Philippines");

// Escape Character (\)
// "\n" refers to creating a new line or set the text to next line;

console.log("line1\nline2");

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);

message = 'John\'s employee went home early';
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);

let grade = 98.7
console.log(grade);

// Exponential number
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);
console.log(2.3 + grade);

// Arrays - stores multiple values with similar data types

let grades = [95.6, 89.3, 96.7, 93.2];
console.log(grades);

// Storing different data types inside an array is not recommended.
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects - are another special kind of data type that is used to mimic real world objects/items

/* Syntax:

	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912 345 6789", "8123 7444"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
};

console.log(person);

const myGrades = {
	firstGrading: 95.6,
	secondGrading: 89.3,
	thirdGrading: 96.7,
	fourthGrading: 93.2
};

console.log(myGrades);

let stringValue = "abcd";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];

// myGrades as object

// 'type of' -
// we use type of operator to retrieve / know the data type

console.log(typeof stringValue); //output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); //output: boolean
console.log(typeof waterBills); //output: object
console.log(typeof myGrades); //output:object

// Constant Objects and arrays
// We cannot reassign the value of the variable, but we can change the elements of the constant array
const anime = ["One piece", "Code Geas", "Prince of Tennis", "Yowamushi no pedal", "Fairy Tale", "Baruto"];

// index - is the position of the element starting zero
anime[0] = "Naruto"
console.log(anime);

// Null - it is used to intentionally express the absence of a value
let spouse = null;
console.log(spouse);

// Undefined - represents the state of a variable that has been declared but without an assigned value
let fullName; // declaration
console.log(fullName);
